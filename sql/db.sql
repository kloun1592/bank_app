CREATE TABLE users 
(
	id INT NOT NULL AUTO_INCREMENT,
	login VARCHAR(255),
	password VARCHAR(255),
	legal_entity_id INT NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (legal_entity_id) REFERENCES bank_app.legal_entity(id)
) ENGINE=InnoDB CHARACTER SET=UTF8;



CREATE TABLE legal_entity 
(
	id INT NOT NULL AUTO_INCREMENT,
	full_name VARCHAR(255),
	legal_address VARCHAR(255),
	INN INT NOT NULL,
	OGRN VARCHAR(13),
	KPP INT NOT NULL,
	bank_id INT NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (bank_id) REFERENCES bank(id)
) ENGINE=InnoDB CHARACTER SET=UTF8;

CREATE TABLE settlement_accounts 
(
	id INT NOT NULL AUTO_INCREMENT,
	account_number INT NOT NULL,
	bank_id INT NOT NULL,
	legal_entity_id INT NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (bank_id) REFERENCES bank(id),
	FOREIGN KEY (legal_entity_id) REFERENCES legal_entity(id)
) ENGINE=InnoDB CHARACTER SET=UTF8;

CREATE TABLE payment_order
(
	id INT NOT NULL AUTO_INCREMENT,
	sender_id INT NOT NULL,
	receiver_id INT NOT NULL,
	payment_type VARCHAR(255),
	order_number INT NOT NULL,
	payment_date DATE NOT NULL,
	payment_priority INT NOT NULL,
	amount INT NOT NULL,
	VAT INT NOT NULL,
	purpose INT NOT NULL,
	status  INT NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (sender_id) REFERENCES legal_entity(id),
	FOREIGN KEY (receiver_id) REFERENCES legal_entity(id)
) ENGINE=InnoDB CHARACTER SET=UTF8;

CREATE TABLE payment_budget
(
	payment_order_id INT NOT NULL,
	UIN INT NOT NULL,
	sender_status VARCHAR(255),
	KBK_code INT NOT NULL,
	OKTMO_code INT NOT NULL,
	FOREIGN KEY (payment_order_id) REFERENCES payment_order(id)
) ENGINE=InnoDB CHARACTER SET=UTF8;

CREATE TABLE payment_tax
(
	payment_order_id INT NOT NULL,
	UIN INT NOT NULL,
	sender_status VARCHAR(255),
	KBK_code INT NOT NULL,
	OKTMO_code INT NOT NULL,
	reason VARCHAR(255),
	document_number INT NOT NULL,
	document_date DATE NOT NULL,
	tax_type VARCHAR(255),
	FOREIGN KEY (payment_order_id) REFERENCES payment_order(id)
) ENGINE=InnoDB CHARACTER SET=UTF8;

CREATE TABLE payment_tax_period
(
	payment_order_id INT NOT NULL,
	tax_period VARCHAR(255),
	number INT NOT NULL,
	year INT NOT NULL,
	FOREIGN KEY (payment_order_id) REFERENCES payment_order(id)
) ENGINE=InnoDB CHARACTER SET=UTF8;

CREATE TABLE payment_tax_date
(
	payment_order_id INT NOT NULL,
	date DATE NOT NULL,
	FOREIGN KEY (payment_order_id) REFERENCES payment_order(id)
) ENGINE=InnoDB CHARACTER SET=UTF8;

CREATE TABLE payment_tax_custom_authority
(
	payment_order_id INT NOT NULL,
	code INT NOT NULL,
	FOREIGN KEY (payment_order_id) REFERENCES payment_order(id)
) ENGINE=InnoDB CHARACTER SET=UTF8;








