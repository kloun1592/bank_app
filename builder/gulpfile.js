'use strict';

var gulp = require('gulp'),
    prefixer = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'),
    sass = require('gulp-sass'),
    cleanCSS = require('gulp-clean-css'),
    sourcemaps = require('gulp-sourcemaps'),
    rigger = require('gulp-rigger'),
    //imagemin = require('gulp-imagemin'),
    //pngquant = require('imagemin-pngquant'),
    connect = require('gulp-connect'),
    htmlmin = require('gulp-htmlmin');

var path = {
    css: {
        src: "../src/css/**/*.scss",
        dest: "../css"
    },

    js: {
        src: "../src/js/**/*.js",
        dest: "../js"
    },

    html: {
        src: "../src/**/*.php",
        dest: "../"
    }
};

gulp.task('connect', function(done) {
  connect.server({
      port: 8888,
      root: '../',
      livereload: true
    })
});

function css()
{
    return gulp.src(path.css.src)
         //.pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(prefixer({
            browsers: ['last 6 versions'],
            cascade: false
        }))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        //.pipe(sourcemaps.write())
        .pipe(gulp.dest(path.css.dest));
}

function html()
{
    return gulp.src(path.html.src)
        .pipe(rigger())
        .pipe(htmlmin({collapseWhitespace: true}))
        .pipe(gulp.dest(path.html.dest))
        //.pipe(connect.reload());
}

function js ()
{
   return gulp.src(path.js.src)
        .pipe(rigger())
        .pipe(sourcemaps.init())
        .pipe(uglify())
        .pipe(gulp.dest(path.js.dest))
        //.pipe(connect.reload());
}

gulp.task('watch:styles', function () {
    gulp.watch(path.css.src, gulp.series(css));
});

gulp.task('watch:scripts', function () {
    gulp.watch(path.js.src, gulp.series(js));
});

gulp.task('watch:html', function () {
    gulp.watch(path.html.src, gulp.series(html));
});

gulp.task('watch', gulp.series(css,
    gulp.parallel('watch:styles', 'watch:scripts', 'watch:html')
));


gulp.task('default', gulp.series(html, css, js, gulp.parallel('watch')));