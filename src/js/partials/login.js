$("#loginForm").submit(function(event)
{
    event.preventDefault();

    var $form = $(this);

    var $inputs = $form.find("input, select, button, textarea");

    var serializedData = $form.serializeArray();

    $inputs.prop("disabled", true);

    $.ajax({
        type: 'POST',
        url : 'php/libs/login.php',
        data: serializedData,
        success: function(data)
        {
            if (data == "success")
            {
                $inputs.prop("disabled", false);
                window.location.href = "/";
            }
            else
            {
                $inputs.prop("disabled", false);
                alert(data);
            }
        },
        error: function(data)
        {
            alert("Error");
            $inputs.prop("disabled", false);
        }
    });

});