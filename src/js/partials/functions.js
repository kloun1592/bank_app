function showErrorMsg()
{
    showDialog({
        title: 'Ошибка! Что-то пошло не по плану....',
        text: ""
    })
}

function openModalWithParsedCompanyInfo(info)
{
    var html = '<ul class="mdl-list">\n' +
        '  <li class="mdl-list__item">\n' +
        '    <span class="mdl-list__item-primary-content">\n' +
        'Полное наименование организации: \n' +
        '    ' + info.full_name + '\n' +
        '</span>\n' +
        '  </li>\n' +
        '  <li class="mdl-list__item">\n' +
        '    <span class="mdl-list__item-primary-content">\n' +
        '    ИНН: \n' +
        '    ' + info.TIN +'\n' +
        '  </span>\n' +
        '  </li>\n' +
        '  <li class="mdl-list__item">\n' +
        '    <span class="mdl-list__item-primary-content">\n' +
        '    ОГРН: \n' +
        '    ' + info.OGRN +'\n' +
        '  </span>\n' +
        '  </li>\n' +
        '  <li class="mdl-list__item">\n' +
        '    <span class="mdl-list__item-primary-content">\n' +
        '    КПП: \n' +
        '    ' + info.CPR +'\n' +
        '  </span>\n' +
        '  </li>\n' +
        '  <li class="mdl-list__item">\n' +
        '    <span class="mdl-list__item-primary-content">\n' +
        '    Расчётный счёт: \n' +
        '    ' + info.checking_account +'\n' +
        '  </span>\n' +
        '  </li>\n' +
        '</ul>'



    showDialog({
        title: 'Реквизиты:',
        text: html
    })
}

function showCompanyInfo(filePath)
{
    var getInfo = $.post(filePath, function( data )
    {
        var info = JSON.parse(data);
        openModalWithParsedCompanyInfo(info[0])

    })
        .fail(function()
        {
            showErrorMsg()
        })
}
