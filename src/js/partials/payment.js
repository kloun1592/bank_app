$(".payment_form").submit(function(event)
{
    event.preventDefault();

    var $form = $(this);

    var $inputs = $form.find("input, select, button, textarea");

    var serializedData = $form.serializeArray();

    $inputs.prop("disabled", true);


    //TODO: Заменить alert'ы на подсказки или модальные окна
    $.ajax({
        type: 'POST',
        url : 'php/payment_creator.php',
        data: serializedData,
        success: function(data)
        {
           alert(data);
            $inputs.prop("disabled", false);
        },
        error: function(data)
        {
           alert(data);
            $inputs.prop("disabled", false);
        }
    });

});