<?php
if (isset($_COOKIE['usr_id']))
{
    header('Location: http://localhost:8888/bank_app/index.php');
}
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>

    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.indigo-pink.min.css">
    <link rel="stylesheet" href="css/main.css">
</head>
<body>
<div class="mdl-card mdl-shadow--3dp login-form">
    <h2 class="mdl-card__title-text">Login</h2>
    <span class="server-answer"></span>

    <form action="#" method="POST" id="loginForm">

        <div class="mdl-textfield mdl-js-textfield">
            <input class="mdl-textfield__input" name="user_login" type="text" id="login">
            <label class="mdl-textfield__label" for="login">Your login...</label>
        </div>

        <div class="mdl-textfield mdl-js-textfield">
            <input class="mdl-textfield__input" name="user_pass" type="password" id="pass">
            <label class="mdl-textfield__label" for="pass">Your password...</label>
        </div>

        <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored">
            Sign in
        </button>
    </form>

    <h5 class="mdl-card__title-text h5">
        Forgot your password ?</h5>
    <a class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored" href="pass_recover.html">Reestablish</a>
</div>


<script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
<script src="js/main.js"></script>
</body>
</html>