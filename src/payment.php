<?php
if (!isset($_COOKIE['usr_id']))
{
    header('Location: http://localhost:8888/bank_app/login.php');
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Main</title>

    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.indigo-pink.min.css">
    <link rel="stylesheet" href="css/main.css">
</head>
<body>
<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
    <header class="mdl-layout__header">
        <div class="mdl-layout__header-row">
            <!-- Title -->
            <span class="mdl-layout-title">Bank</span>
            <!-- Add spacer, to align navigation to the right -->
            <div class="mdl-layout-spacer"></div>
            <!-- Navigation. We hide it in small screens. -->
            <nav class="mdl-navigation mdl-layout--large-screen-only">
                <a class="mdl-navigation__link" href="">Main</a>
                <a class="mdl-navigation__link" href="">Link</a>
                <a class="mdl-navigation__link" href="">Link</a>
                <a class="mdl-navigation__link" href="">Link</a>
            </nav>
        </div>
    </header>
    <div class="mdl-layout__drawer">
        <span class="mdl-layout-title">Bank</span>
        <nav class="mdl-navigation">
            <a class="mdl-navigation__link" href="">Main</a>
            <a class="mdl-navigation__link" href="">Link</a>
            <a class="mdl-navigation__link" href="">Link</a>
            <a class="mdl-navigation__link" href="">Link</a>
        </nav>
    </div>
    <main class="mdl-layout__content">
        <div class="page-content">
            <div class="mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet account-info-block">
                <div class="account-info-block__header">
                    <h2 class="mdl-card__title-text with-action">Платеж в рублях</h2>
                </div>
                <div class="account-info-block__main">
                    <form class="payment_form" action="">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <input class="mdl-textfield__input" type="text" id="receiver_name" name="receiver_name">
                            <label class="mdl-textfield__label" for="receiver_name">Наименование контрагента/получателя/ФИО</label>
                            <span class="mdl-textfield__error">Недопустимый формат</span>
                        </div>
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <input class="mdl-textfield__input" type="text" pattern="-?[0-9]*(\.[0-9]+)?" id="reciever_settlement_account" name="reciever_settlement_account">
                            <label class="mdl-textfield__label" for="reciever_settlement_account">Счёт получателя</label>
                            <span class="mdl-textfield__error">Недопустимый формат</span>
                        </div>
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <input class="mdl-textfield__input" type="text" pattern="-?[0-9]*(\.[0-9]+)?" id="reciever_INN" name="reciever_INN">
                            <label class="mdl-textfield__label" for="reciever_INN">ИНН контрагента/получателя</label>
                            <span class="mdl-textfield__error">Недопустимый формат</span>
                        </div>
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <input class="mdl-textfield__input" type="text" pattern="-?[0-9]*(\.[0-9]+)?" id="reciever_KPP" name="reciever_KPP">
                            <label class="mdl-textfield__label" for="reciever_KPP">КПП контрагента/получателя</label>
                            <span class="mdl-textfield__error">Недопустимый формат</span>
                        </div>
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <input class="mdl-textfield__input" type="text" id="reciever_bank_name" name="reciever_bank_name">
                            <label class="mdl-textfield__label" for="reciever_bank_name">Наименование банка контрагента/получателя</label>
                            <span class="mdl-textfield__error">Недопустимый формат</span>
                        </div>
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <input class="mdl-textfield__input" type="text" pattern="-?[0-9]*(\.[0-9]+)?" id="reciever_bank_BIK" name="reciever_bank_BIK">
                            <label class="mdl-textfield__label" for="reciever_bank_BIK">БИК банка получателя</label>
                            <span class="mdl-textfield__error">Недопустимый формат</span>
                        </div>

                        <h5>Тип платежа:</h5>
                        <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="option-1">
                            <input type="radio" id="option-1" class="mdl-radio__button" name="payment_type" value="user" checked>
                            <span class="mdl-radio__label">Контрагент</span>
                        </label>
                        <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="option-2">
                            <input type="radio" id="option-2" class="mdl-radio__button" name="payment_type" value="budget">
                            <span class="mdl-radio__label">Бюджет</span>
                        </label>
                        <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="option-3">
                            <input type="radio" id="option-3" class="mdl-radio__button" name="payment_type" value="tax">
                            <span class="mdl-radio__label">Налоги и взносы</span>
                        </label>

                        <h5>Очередь платежа:</h5>
                        <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="option-11">
                            <input type="radio" id="option-11" class="mdl-radio__button" name="order_number" value="1" checked>
                            <span class="mdl-radio__label">1</span>
                        </label>
                        <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="option-22">
                            <input type="radio" id="option-22" class="mdl-radio__button" name="order_number" value="2">
                            <span class="mdl-radio__label">2</span>
                        </label>
                        <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="option-33">
                            <input type="radio" id="option-33" class="mdl-radio__button" name="order_number" value="3">
                            <span class="mdl-radio__label">3</span>
                        </label>
                        <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="option-44">
                            <input type="radio" id="option-44" class="mdl-radio__button" name="order_number" value="4">
                            <span class="mdl-radio__label">4</span>
                        </label>
                        <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="option-55">
                            <input type="radio" id="option-55" class="mdl-radio__button" name="order_number" value="5">
                            <span class="mdl-radio__label">5</span>
                        </label>

                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <input class="mdl-textfield__input" type="text" pattern="-?[0-9]*(\.[0-9]+)?" id="amount" name="amount">
                            <label class="mdl-textfield__label" for="amount">Сумма</label>
                            <span class="mdl-textfield__error">Недопустимый формат</span>
                        </div>
                        <div class="mdl-textfield mdl-js-textfield">
                            <textarea class="mdl-textfield__input" rows= "3" id="purpose" name="purpose"></textarea>
                            <label class="mdl-textfield__label" for="purpose">Назначение платежа</label>
                        </div>
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <input class="mdl-textfield__input" type="text" pattern="^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]|(?:Jan|Mar|May|Jul|Aug|Oct|Dec)))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2]|(?:Jan|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec))\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)(?:0?2|(?:Feb))\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9]|(?:Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep))|(?:1[0-2]|(?:Oct|Nov|Dec)))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$" id="order_date" name="order_date">
                            <label class="mdl-textfield__label" for="order_date">Дата платежа:</label>
                            <span class="mdl-textfield__error">Недопустимый формат</span>
                        </div>

                        <button class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect">
                            Отправить
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </main>
</div>
<script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
<script src="js/main.js"></script>
</body>
</html>