<?php
    require_once "../bootstrap.php";
    require_once "CDatabase.php";
    require_once "libs/num2str.php";

    use PhpOffice\PhpWord\Settings;

    function getLegalEntity()
    {
        $db = new Database();

        $db->dbInitialConnect("bank_app_users");

        $query = "SELECT `legal_entity_id` FROM `users` WHERE login = '" . $_COOKIE["usr_id"] . "'";

        $results = $db->dbQueryGetResult($query);

        $db->dbConnectClose();

        return $results;
    }

    function createDocument($documentData)
    {
        // TODO: Реализовать формирование названия файла
        // TODO: Продумать задачу с id для кадлого платежа
        // TODO: Вытащить данные отправителя и добавить их в шаблон документа
        // TODO: Сделать проверку на MYSQL-инъекции

        $fileName  = 'template.docx';
        $str_num = num2str($_POST["amount"]);

        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($fileName);
        $templateProcessor->setValue(array('id',//
                                           'order_date',//
                                            'amount_text',
                                            'amount',//
                                            'sender_INN',// bd
                                            'sender_KPP',// bd
                                            'sender_name',// bd
                                            'sender_bank_name',// bd
                                            'sender_settlement_account',// bd
                                            'sender_bank_BIK',// bd
                                            'sender_bank_correspondent_account', // bd
                                            'reciever_bank_name', //
                                            'reciever_bank_BIK',//
                                            'reciever_settlement_account',//
                                            'reciever_INN',//
                                            'reciever_KPP',//
                                            'receiver_name',//
                                            'order_number',//
                                            'purpose'),//
                                        array(rand(5, 15),
                                            $_POST["order_date"],
                                            $str_num,
                                            $_POST["amount"],
                                            "sender_INN",
                                            'sender_KPP',
                                            'sender_name',
                                            'sender_bank_name',
                                            'sender_settlement_account',
                                            'sender_bank_BIK',
                                            'sender_bank_correspondent_account',
                                            $_POST["reciever_bank_name"],
                                            $_POST["reciever_bank_BIK"],
                                            $_POST["reciever_settlement_account"],
                                            $_POST["reciever_INN"],
                                            $_POST["reciever_KPP"],
                                            $_POST["receiver_name"],
                                            $_POST["order_number"],
                                            $_POST["purpose"]
                                            ));
        $templateProcessor->saveAs('results/' . $fileName);

    }

    echo (var_dump($_POST));
    //TODO: Улучшить текущую проверку, ибо сейчас она никакая
    if (!empty($_POST))
    {
        createDocument($_POST);
    }
    else
    {
        echo "Что-то пошло не по плану...";
    }
