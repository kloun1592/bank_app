<?php
    require_once "libs/consts.php";

    class Database
    {
        private $dbLink = NULL;

        public function dbInitialConnect($dbName)
        {
            $this->dbLink = mysqli_connect(HOST, USER, PASS, $dbName);

            $error = mysqli_connect_error();
            if ($error)
            {
                die('Unable to connect to database');
            }
        }

        public function dbConnectClose()
        {
            mysqli_close($this->dbLink);
        }

        public function dbQuery($query)
        {
            $result = mysqli_query($this->dbLink, $query);
            return $result;
        }

        public function dbQueryGetResult($query)
        {
            $data = array();

            if ($result = mysqli_query($this->dbLink, $query))
            {
                while ($row = mysqli_fetch_assoc($result))
                {
                    array_push($data, $row);
                }
            }

            mysqli_free_result($result);

            return $data;
        }

        // TODO: Починить защиту от инъекций
        public function destroyInjection($injectableString)
        {
            return mysqli_real_escape_string($this->dbLink, $injectableString);
        }

    }